#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/passwd/Sanity/smoke-test
#   Description: Test basic functionality.
#   Author: Ondrej Moris <omoris@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2010 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include rhts environment
. /usr/bin/rhts-environment.sh
. /usr/share/beakerlib/beakerlib.sh

PACKAGE="passwd"

function set_password {

expect >/tmp/out 2>&1 <<EOF
spawn passwd $1
set timeout 3
expect "password:"
send "$2\r"
expect "password:"
send "$2\r"
expect "successfully" { exit 0 }
exit 2
EOF

ret=$?
cat /tmp/out
return $ret
}

function check_password {

expect >/tmp/out 2>&1 <<EOF
spawn ssh $1@`hostname` echo -n "XYZ" && whoami
set timeout 3
expect "(yes/no)" { send "yes\r" }
expect "password" {
  send "$2\r"
  expect "XYZ${1}" { exit 0 }
  exit 2
}
exit 3
EOF

ret=$?
cat /tmp/out
return $ret
}

rlJournalStart

    rlPhaseStartSetup

    I=0
	while true; do
        getent passwd t${I} || break
	    I=$[$I+1]
	done
	U=t${I}
	rlRun "useradd $U" 0 "Creating testing account"

    rlPhaseEnd

    rlPhaseStartTest

    rlRun "set_password $U a" 0 "Setting initial password to 'a'"
	sleep 3
	rlRun "check_password $U a" 0 "Checking good password"
	rlRun "check_password $U x" 1-255 "Checking wrong password"
	rlRun "echo b | passwd --stdin $U" 0 "Changing password via stdin to 'b'"
	sleep 3
	rlRun "check_password $U b" 0 "Checking good password"
	rlRun "check_password $U x" 1-255 "Checking wrong password"
	rlRun "passwd -l $U" 0 "Locking account"
	sleep 3
	rlRun "check_password $U b" 1-255 "Checking good password"
	rlRun "check_password $U x" 1-255 "Checking wrong password"
	rlRun "passwd -u $U" 0 "Unlocking account"
	sleep 3
	rlRun "check_password $U b" 0 "Checking good password"
	rlRun "check_password $U x" 1-255 "Checking wrong password"
	rlRun "passwd -d $U" 0 "Disabling account"
	sleep 3
	rlRun "check_password $U b" 1-255 "Checking good password"
	rlRun "check_password $U x" 1-255 "Checking wrong password"
	rlRun "passwd -l $U" 0 "Locking account"
	sleep 3
	rlRun "check_password $U x" 1-255 "Checking wrong password"
	rlRun "passwd -u $U > /tmp/out 2>&1" 1-255 "Unlocking account"
	rlRun "grep \"Unsafe\" /tmp/out" 0 "Checking warning message"
	rlRun "passwd -uf $U" 0 "Force Unlocking account"
	sleep 3
	rlRun "echo c | passwd --stdin $U" 0 "Changing password via stdin to 'c'"
	sleep 3
	rlRun "check_password $U c" 0 "Checking good password"
	rlRun "check_password $U x" 1-255 "Checking wrong password"
	rlRun "passwd -d $U" 0 "Disabling account"
	sleep 3
	rlRun "check_password $U c" 1-255 "Checking good password"
	rlRun "check_password $U x" 1-255 "Checking wrong password"
	rlRun "echo d | passwd --stdin $U" 0 "Changing password via stdin to 'd'"
	sleep 3
	rlRun "check_password $U d" 0 "Checking good password"
	rlRun "check_password $U x" 1-255 "Checking wrong password"
	rlRun "passwd -S $U" 0 "Checking good account information"
	rlRun "passwd -S nonexistinguser" 1-255 "Checking wrong account information"

    rlPhaseEnd

    rlPhaseStartCleanup

    rlRun "userdel -r $U" 0 "Removing testing account"
    rlRun "rm -f /tmp/out" 0 "Removing output file"

    rlPhaseEnd

rlJournalPrintText

rlJournalEnd
