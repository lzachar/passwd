#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/passwd/Regression/changing-password-with-stdin-opiton-limits
#   Description: Test for changing password with --stdin opiton limits
#   Author: Eva Mrakova <emrakova@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2015 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="passwd"

TSTUSR="testuser"
# max length password: 512 chars (supported by pam_unix, PAM_MAX_RESP_SIZE)
# such long passwords do not work on RHEL6 for pam <= pam-1.1.1-20.el6
# similarly for RHEL7 and pam-1.1.8-12.el7
TSTPASSWD=$(tr -dc 'A-Za-z0-9' </dev/urandom | head -c 511)
NEWPASSWD=$(tr -dc 'A-Za-z0-9' </dev/urandom | head -c 511)

function change_passwd {
    # $1 user, $2 user's password, $3 new user's password
    expect <<EOF
spawn su -c passwd - $1
set timeout 3
expect "password:"
send "$2\r"
expect "password:"
send "$3\r"
expect "password:"
send "$3\r"
expect "successfully" { exit 0 }
exit 2
EOF
    return $?
}

function set_passwd {
    # $1 user, $2 passwd
    expect <<EOF
spawn passwd $1
set timeout 3
expect "password:"
send "$2\r"
expect "password:"
send "$2\r"
expect "successfully" { exit 0 }
exit 2
EOF
    return $?
}

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"
        rlRun "useradd $TSTUSR"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "set_passwd $TSTUSR $TSTPASSWD" 0 "root: setting password interactively"
        rlRun "change_passwd $TSTUSR $TSTPASSWD $NEWPASSWD" 0 "user: changing password"
        rlRun "echo $TSTPASSWD | passwd --stdin $TSTUSR" 0 "root: resetting password via --stdin"
        rlRun "change_passwd $TSTUSR $TSTPASSWD $NEWPASSWD" 0 "user: changing password"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "userdel -r $TSTUSR"
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
